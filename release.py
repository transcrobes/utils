# -*- coding: utf-8 -*-

"""
Based heavily on https://gitlab.com/alelec/gitlab-release/ but
this script uses the python-gitlab library instead of calling
the API with requests directly
The idea is to support new Gitlab features as soon as they
come out (like Release Links in Gitlab 11.8).
"""

import os
import logging
import zipfile
import argparse
import itertools
from glob import glob
from urllib.parse import quote, urlparse
import gitlab

logging.basicConfig(level=os.getenv('LOG_LEVEL') or logging.WARNING)


def release(args):
    uri = urlparse(args.project_url)
    server = f'{uri.scheme}://{uri.netloc}'
    project_id = quote(uri.path[1:], safe='')
    gl = gitlab.Gitlab(server, private_token=args.private_token, timeout=args.timeout,
                       ssl_verify=args.ssl_verify)
    project = gl.projects.get(project_id)
    release_notes = []
    if args.description:
        release_notes.append(args.description)
    for fname in args.all_files:
        uploaded_file = project.upload(os.path.basename(fname), filepath=fname)
        logging.debug(f'Got back from upload: {uploaded_file}')
        release_notes.append(uploaded_file["markdown"])

    tag = project.tags.get(args.release_tag)
    if tag.release and tag.release["description"]:
        release_notes = [tag.release["description"]] + release_notes
    tag.set_release_description(' \n'.join(release_notes))
    project.save()

def main():

    parser = argparse.ArgumentParser(description='Upload files to gitlab tag (release)')
    parser.add_argument('--project_url', default=None
                        , help='Full url of the project on the gitlab server or $CI_PROJECT_URL')
    parser.add_argument('--release_tag', default=None, help='Tag to upload files against or $CI_BUILD_TAG')
    parser.add_argument('--timeout', type=int, default=120, help='Timeout for http requests')
    parser.add_argument('--ssl_verify', action="store_false", help='Verify SSL certificates')
    parser.add_argument('--zip', help='Add all globbed files to provided zip name and upload that')
    parser.add_argument('--description', default='', help='Extra text to add to the release description')
    parser.add_argument('private_token', help='login token with permissions to commit to repo')
    parser.add_argument('files', nargs="+", help='glob/s of files to upload')

    args = parser.parse_args()

    args.project_url = args.project_url or os.environ.get('CI_PROJECT_URL')
    if not args.project_url:
        print("Must provide --project_url if not running from CI")
        exit(1)
    logging.debug(f'Using {args.project_url} for value "project_url"')

    args.release_tag = args.release_tag or os.environ.get('CI_BUILD_TAG')
    if not args.release_tag:
        print("Must provide --release_tag if not running from CI")
        exit(1)
    logging.debug(f'Using {args.release_tag} for value "release_tag"')

    logging.info(f"Uploading to {args.project_url} @ {args.release_tag}")

    all_files = list(itertools.chain(*[glob(f) for f in args.files]))

    if args.zip:
        with zipfile.ZipFile(args.zip, "w", zipfile.ZIP_DEFLATED) as zf:
            def zipdir(path, ziph):
                # ziph is zipfile handle
                for root, dirs, files in os.walk(path):
                    for file in files:
                        ziph.write(os.path.join(root, file))

            for fname in all_files:
                print (fname)
                if fname == args.zip:
                    continue
                if os.path.isdir(fname):
                    zipdir(fname, zf)
                else:
                    zf.write(fname)

        all_files = [os.path.abspath(args.zip)]

    logging.info(f"Uploading {all_files}")
    args.all_files = all_files

    release(args)

    logging.info(f"Uploaded {args.all_files} to tag {args.release_tag}")


if __name__ == '__main__':
    main()
